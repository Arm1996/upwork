/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100137
Source Host           : localhost:3306
Source Database       : lar1

Target Server Type    : MYSQL
Target Server Version : 100137
File Encoding         : 65001

Date: 2019-04-25 05:54:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `product_id` int(20) NOT NULL,
  `piece` int(20) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cart
-- ----------------------------
INSERT INTO `cart` VALUES ('23', '11', '2', '2');
INSERT INTO `cart` VALUES ('24', '24', '2', '2');
INSERT INTO `cart` VALUES ('25', '11', '8', '2');

-- ----------------------------
-- Table structure for feedback
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `product_id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `text` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `feedback_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of feedback
-- ----------------------------
INSERT INTO `feedback` VALUES ('1', '2', '11', 'asdads');
INSERT INTO `feedback` VALUES ('2', '2', '11', 'asddsa');
INSERT INTO `feedback` VALUES ('3', '3', '12', 'ayooo');

-- ----------------------------
-- Table structure for notif
-- ----------------------------
DROP TABLE IF EXISTS `notif`;
CREATE TABLE `notif` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `text` varchar(255) NOT NULL DEFAULT '',
  `product_id` int(20) NOT NULL,
  `status` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `notif_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `notif_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of notif
-- ----------------------------
INSERT INTO `notif` VALUES ('1', '11', '', '6', '1');
INSERT INTO `notif` VALUES ('2', '11', '', '7', '1');
INSERT INTO `notif` VALUES ('3', '25', '', '8', '1');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `sum` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `order_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES ('2', '2019-04-24 01:17:39.885361', '910000', '11');
INSERT INTO `order` VALUES ('3', '2019-04-24 19:28:36.457945', '924000', '11');

-- ----------------------------
-- Table structure for order_details
-- ----------------------------
DROP TABLE IF EXISTS `order_details`;
CREATE TABLE `order_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `order_id` int(20) NOT NULL,
  `product_id` int(20) NOT NULL,
  `price` int(20) NOT NULL,
  `count` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `order_details_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order_details
-- ----------------------------
INSERT INTO `order_details` VALUES ('2', '2', '2', '455000', '2');
INSERT INTO `order_details` VALUES ('3', '3', '2', '455000', '2');
INSERT INTO `order_details` VALUES ('4', '3', '8', '14000', '1');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `count` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('2', 'Bmw', '2', '455000', 'yoooo', '12', '1');
INSERT INTO `products` VALUES ('3', 'Yacht', '6', '4999000', 'Cool Yacht', '11', '1');
INSERT INTO `products` VALUES ('4', 'botas', '1', '75500', 'aaaa', '11', '1');
INSERT INTO `products` VALUES ('5', 'botas', '1', '75500', 'asdsda', '11', '1');
INSERT INTO `products` VALUES ('6', 'botas5', '1', '15300', 'asd', '11', '1');
INSERT INTO `products` VALUES ('7', 'botas5', '1', '15300', 'saas', '11', '1');
INSERT INTO `products` VALUES ('8', 'botas', '31', '14000', 'asdasd', '25', '1');

-- ----------------------------
-- Table structure for products_photos
-- ----------------------------
DROP TABLE IF EXISTS `products_photos`;
CREATE TABLE `products_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `products_photos_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of products_photos
-- ----------------------------
INSERT INTO `products_photos` VALUES ('4', '2', '13029604_499354196938793_7183064618772093743_o_large.jpg');
INSERT INTO `products_photos` VALUES ('5', '2', '13047900_499354166938796_8725787177037763974_o_1024x1024.jpg');
INSERT INTO `products_photos` VALUES ('6', '2', 'images.jpg');
INSERT INTO `products_photos` VALUES ('7', '3', '4fe384c5.jpg');
INSERT INTO `products_photos` VALUES ('8', '3', 'c97e22478084588f91bdaf4a46661c6340d23060.jpg');
INSERT INTO `products_photos` VALUES ('9', '3', 'maxresdefault.jpg');
INSERT INTO `products_photos` VALUES ('10', '3', 'xTropical-Island-Paradise.jpg.pagespeed.ic.pfS_VtTp03.jpg');
INSERT INTO `products_photos` VALUES ('11', '4', 'O1CN01PW4QUk1MJeX9R6oml_!!1035071414.jpg');
INSERT INTO `products_photos` VALUES ('12', '5', 'O1CN011E1m6sjZgnDbfrG_!!3826690292.jpg');
INSERT INTO `products_photos` VALUES ('13', '6', 'O1CN019Ew8I91orpJIWCJZp_!!1822795279.jpg');
INSERT INTO `products_photos` VALUES ('14', '7', 'O1CN010Im0SR1ezwBESVZ27_!!705133943.jpg');
INSERT INTO `products_photos` VALUES ('15', '8', 'TB2RVqcghPI8KJjSspoXXX6MFXa_!!825104382.jpg');
INSERT INTO `products_photos` VALUES ('16', '8', 'TB2xRJJgnTI8KJjSsphXXcFppXa_!!825104382.jpg');
INSERT INTO `products_photos` VALUES ('17', '8', 'TB2xRJJgnTI8KJjSsphXXcFppXa_!!8251043854452.jpg');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `age` int(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'user',
  `photo` varchar(255) NOT NULL DEFAULT '',
  `lastvisit` varchar(255) NOT NULL DEFAULT '',
  `blocktime` int(20) NOT NULL DEFAULT '0',
  `verify` int(20) NOT NULL DEFAULT '0',
  `recovercode` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('11', 'Muhammed', 'Salah', '33', 'salah@mail.ru', '$2y$10$6Yg8aMVKn/yrlVzlHYCQouXpZUeoHYj9PvPPYeYn/67jE2ttpENzO', 'user', '1556150529.jpg', '25-04-2019 02:26:59am', '1555077493', '1', '');
INSERT INTO `user` VALUES ('12', 'admin', 'adminov', '23', 'admin@mail.ru', '$2y$10$F0q8CNtLwSpjmhvOF7yT.OAaGYCYga4LVh81s4l4AFkH9T5TlzpnO', 'admin', '', '24-04-2019 02:18:37am', '0', '1', '');
INSERT INTO `user` VALUES ('15', 'Babken', 'Gzraryan', '21', 'davona1@mial.ru', '$2y$10$BNsGEtygcOlMcXefKN5MN.cE45eG18Q9J4rt5IdOztDaBGhRP0yhu', 'user', '', '', '0', '1', '');
INSERT INTO `user` VALUES ('24', 'Babken', 'Vardanyan', '56', 'watchme9596@gmail.com', '$2y$10$JO.LzgMJoukeYD9SrQOC9OeKkM3j5S1WrGPxy3KyGiGuwpMPS0E6y', 'user', '', '17-04-2019 18:46:47pm', '0', '1', 'aaaa');
INSERT INTO `user` VALUES ('25', 'Armen', 'Meltonyan', '23', 'arm@mail.ru', '$2y$10$Fr1twA2/DIYOhabNVLxga.52cigwWFUGRM5Vdn/yCQ0SEc99j.BKy', 'user', '', '24-04-2019 19:27:53pm', '0', '1', '0');

-- ----------------------------
-- Table structure for wishlist
-- ----------------------------
DROP TABLE IF EXISTS `wishlist`;
CREATE TABLE `wishlist` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `product_id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `wishlist_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `wishlist_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wishlist
-- ----------------------------
INSERT INTO `wishlist` VALUES ('3', '2', '11');
INSERT INTO `wishlist` VALUES ('4', '3', '11');
SET FOREIGN_KEY_CHECKS=1;
