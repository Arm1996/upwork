<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Usermodel;
use Session;
use App\CartModel;
use App\ProductModel;
use App\WishlistModel;
class Cart extends Controller
{
    function addtocart(Request $r){
        $id = $r->pr_id;
        $productcount  = CartModel::where("product_id",$id)->first();
        $stokcount = ProductModel::where("id",$id)->get('count');
        $products = CartModel::where('user_id',session('id'))->where('product_id',$id)->get();
        if ($productcount['piece'] != $stokcount[0]['count']) {    
            if (count($products) != 0) {
                if ($r->type == 1) {
                   WishlistModel::where('product_id',$id)->where('user_id',session('id'))->delete();
                   CartModel::where('user_id',session('id'))->where('product_id',$id)->update(['piece'=>$productcount['piece']+1]);
               }
               if ($r->type == 2) {
                   CartModel::where('user_id',session('id'))->where('product_id',$id)->update(['piece'=>$productcount['piece']+1]);
               }
           }
           else{
                if ($r->type == 1) {
                   CartModel::insert(["user_id" => session('id'),"product_id" => $id]);
                   WishlistModel::where('product_id',$id)->where('user_id',session('id'))->delete();
                }
                else if ($r->type == 2) {
                   CartModel::insert(["user_id" => session('id'),"product_id" => $id]);
                     // CartModel::where('user_id',session('id'))->where('product_id',$id)->delete();
                }

            }
            return 'true';
        }
        else{
            return 'false';

        }
}
function cartpage(){
   $products = CartModel::where('user_id', session('id'))->get();
   return view('mycart')->with('title','MyCart')->with('cartprods',$products);
}
function mincount(Request $r){
    $id = $r->id;
    $product  = CartModel::where("product_id",$id)->first();
    CartModel::where('product_id',$id)->update(['piece' =>$product['piece']-1]);
    if ($product['piece'] == 0) {
        CartModel::where('product_id',$id)->delete();
        return "false";

    }
    else{
        return "true";
    }
}
function pluscount(Request $r){
    $id = $r->id;
    $product  = CartModel::where("product_id",$id)->first();
    $stokcount = ProductModel::where("id",$id)->get('count');
    if ($product['piece'] != $stokcount[0]['count']) {
        CartModel::where('product_id',$id)->update(['piece' =>$product['piece']+1]);
        return "true";
    }
    else{
        return "false";

    }
}

}
