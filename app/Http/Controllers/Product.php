<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductModel;
use App\photomodel;
use Illuminate\Support\Facades\Redirect;
use Session;
use Validator;
use App\FeedModel;
class product extends Controller
{
    function addproduct(){
    	return view('addproduct')->with('title','addproduct');
    }
    function add(Request $r){
         $validate = Validator::make($r->all(),[
            "name"=>"required|min:3",
            "price"=>"required|integer|min:3",
            "count"=>"required|integer|",
            "desc"=>"required",
        ]);
            if ($validate->fails())
        {
           return Redirect::to("/newprod")->withErrors($validate)->withInput();
        }
        else{
    	// dd($r);
    	$p = new ProductModel();
    	$p->name = $r->prname;
    	$p->price = $r->price;
    	$p->count = $r->count;
    	$p->description = $r->desc;
    	$p->user_id = session('id');
    	$p->save();
    	// dd($p->id);

    	if($r->hasfile('photo'))
	     {
	        foreach($r->file('photo') as $file)
	        {
	            $name=$file->getClientOriginalName();
	            $file->move(public_path().'/prodimages/', $name);  
	            photomodel::insert([
	            	'product_id'=>$p->id,
	            	'url'=>$name,

	            ]);
	        }
	     }
	     Session::flash("success","Hajoxutyamb verbernvec");
	     return Redirect::back();
        }
    }
    function allprod(){
    	$my_product = ProductModel::where("user_id","!=", session("id"))->where(["status"=>1])->get();/**/
    	// dd($my_product[0]->user);
        // dd($my_product->feedbacks); 
    	return view('allproduct')->with('title','allproducts')->with('prods',$my_product);
    }
    function myprod(){
        $my_product = ProductModel::where(["user_id" => session('id')])->get();
        // dd($my_product);
        // dd($my_product[0]->photos[0]->url);
        return view('myprod')->with('title','myproducts')->with('prods',$my_product);
    }

    function product_item(Request $r){
        // dd($r->id);
        $my_product = ProductModel::where(["id" => $r->id])->first();
        // dd($my_product);
        return view('itemdetail')->with('title','prodetail')->with('product',$my_product);
    }
    function editprod(Request $r){
         $validate = Validator::make($r->all(),[
            "name"=>"required|min:3",
            "count"=>"required|integer|",
            "price"=>"required|integer|",
            "desc"=>"required",
        ]);
         if ($validate->fails())
        {
            // dd("erorrr");
            Session::flash("display","true");
           return Redirect::back()->withErrors($validate)->withInput();
        }
        // else{
        $name = $r->name;
        $count = $r->count;
        $price = $r->price;
        $desc = $r->desc;
        $id    = $r->id;

        Productmodel::where("id",$id)->update(["name"=>$name,"count"=>$count,"price"=>$price,"description"=>$desc]);
        // }
        // dd($r->id);
            if($r->hasfile('photo1'))
         {
            foreach($r->file('photo1') as $file)
            {
                $name=$file->getClientOriginalName();
                $file->move(public_path().'/prodimages/', $name);  
                photomodel::insert([
                    'product_id'=>$id,
                    'url'=>$name,

                ]);
            }
         }
     return Redirect::back();    
    }
    function deleteprod(Request $r){
        $id = $r->id;
        Productmodel::where("id",$id)->delete();
    }
    function deletprodpic(Request $r){
        $id = $r->id;
        photomodel::where("id",$id)->delete();
    }
}
