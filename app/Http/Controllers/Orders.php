<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usermodel;
use Session;
use App\CartModel;
use App\ProductModel;
use App\OrderModel;
use App\Order_detailsModel;
use App\FeedModel;
class Orders extends Controller
{
   function getview(){
   	$orders = OrderModel::where('user_id',session('id'))->get();
   	return view('orders')->with('title','My Orders')->with('order',$orders);
   }

   function details(Request $r){
	$orders = Order_detailsModel::where('order_id',$r->id)->get();
   	$arr = [];
   	$prod = [];
   	foreach ($orders as $value) {	
   		$arr[] = $value->product_id;
   	}
   	for ($i=0; $i < count($arr) ; $i++) { 
   		$prod[] = ProductModel::where('id',$arr[$i])->first();
   	}	
   	// dd($prod);
   	return view('details')->with('title','Order details')->with('detail2',$prod);
   }

   function sendfeed(Request $r) {
   	$text = $r->text;
   	$id = $r->id;
   	FeedModel::insert(['user_id'=>session('id'),'product_id'=>$id,'text'=>$text]);
   }
}
