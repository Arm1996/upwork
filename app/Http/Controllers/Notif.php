<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NotifModel;
use Session;
class Notif extends Controller
{
        function notification(){
       	$note = NotifModel::where("user_id",session('id'))->get();
        return view('notification')->with('title','Notifications')->with('notif',$note);
    }
    	function getnotif(){
    		$note = NotifModel::where("user_id",session('id'))->get();
    		return $note;
    	}
    	function delnotif(Request $r){
    		NotifModel::where('id',$r->id)->delete();
    	}
}
