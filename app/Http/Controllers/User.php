<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Validator;
use App\Usermodel;
use Session;
use Illuminate\Support\Facades\Hash;
use App\CartModel;
use Mail;
use Illuminate\Support\Facades\Auth;
class User extends Controller
{
    function homepage(){
        return view("home")->with('title','Home');
    }
    function index(){
    	return view("signup")->with("users",["Anna","Ashot"])->with('title','Signup');
    }

    function login(){
        return view("login")->with('title','Login');
    }

    function profile(){
        $user = Usermodel::where(["id"=>Session::get('id')])->first();
        // dd($user->cartprod);
        return view("profile")->with("user",$user)->with('title',$user['name'].' '.$user['surname'])->with('pic',$user['photo']);
    }
    function forgotpass(){
        return view('forgotpass')->with('title','RecoverPass');
    }

    function add(Request $r){
        $validate = Validator::make($r->all(),[
            "name"=>"required|min:3",
            "surname"=>"required|min:3",
            "age"=>"required|integer|max:110",
            "email"=>"unique:user,email|required|email",
            "password"=>"min:8|alpha_num|required",
            "password2"=>"required|same:password",
        ]);

        if ($validate->fails())
        {
    	   return Redirect::to("/signup")->withErrors($validate)->withInput();
        }
        else{
            $user = new Usermodel;
            $user->name      = $r->input("name");
            $user->surname   = $r->input("surname");
            $user->age       = $r->input("age");
            $user->email     = $r->input("email");
            $user->password  = Hash::make($r->input("password"));
            $user->save();
            $user_id = $user->id;
            $email = $user->email;
            $hash = md5($user_id.$email);
            $data = array('name'=>$user->name,'hash'=>$hash,'id'=>$user_id);
             Mail::send('mail', $data, function($message) use($email){
                $message->to($email, 'Administrator of armproject.am')->subject
                ('Account Verifing');
                $message->from('lilit4020@gmail.com','Lilitik');
            });
               
   }
            Session::flash("success","You are registered successfuly,please check your email!;");
            return Redirect::to('/login');  
        }
        // return view("signup")->with("users",["Anna","Ashot"])->with('title','signup');sxala senc vortev chi tanum et ejy ayl uxaki voncor kpcni kody, include-i pes;
    
   
    function login2(Request $r){
        $email = $r->input("email");
        $password = $r->input("password");
        $userdata = Usermodel::where(["email"=>$email])->first();
        // dd($userdata->status);
        if (!empty($userdata)) {
            if ($userdata->verify == 0) {
               Session::flash('success','Please verify your account');
                return Redirect::to('/login');
            }
              if ($userdata->blocktime > time()) {
            Session::flash('success',"you are blocked");
            return Redirect::to('/login');
        }
            if (Hash::check($password,$userdata['password'])) {
                Session::put("id",$userdata['id']);
                return ($userdata['type'] == 'user')?Redirect::to('/profile'):Redirect::to('/admin');
            }
            else{
                Session::flash("success","sxal e passwordy");
                return Redirect::to('/login');
            }
        }
        else{
            Session::flash("success","sxal e emaily");
            return Redirect::to('/login');
        }

    }
  
    function logout(){
        date_default_timezone_set("Asia/Tbilisi");
        $date = date('d-m-Y H:i:sa');
        Usermodel::where("id",session("id"))->update(['lastvisit' => $date]);
        Session::flush();
        return Redirect::to('/login');
    }
    function upload(Request $r){
        if ($r->hasFile('input_img')) {
        $image = $r->file('input_img');
        $name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/images');
        $image->move($destinationPath, $name);  
        Usermodel::where("id",session("id"))->update(["photo"=>$name]);
        return back()->with('success','Image Upload successfully');
    }
 }
    function edit(Request $r){
        var_dump($r->input("name"));
        $name = $r->input("name");
        $surname = $r->input("surname");
        Usermodel::where("id",session("id"))->update(["name"=>$name,"surname"=>$surname]);
    }
    function verify($hash,$id){
        $user = Usermodel::where("id",$id)->first();
        if ($user){
            // dd($user->email);
            if (md5($id.$user['email'])==$hash) {
                // dd("ayo");
                Usermodel::where("id",$id)->update(["verify"=>1]);
                 Session::flash("success","you are verified successful.Please log in");
                 return Redirect::to('/login');
            }
        }
    }
    function sendemail(Request $r){
        $email = $r->email;
        $user = Usermodel::where("email",$email)->first();
        // print json_encode($user);
        $hash = md5($user->id.$email);
        Usermodel::where("id",$user->id)->update(["recovercode" => $hash]);
        $data = array('name'=>$user->name,'hash'=>$hash,'id'=>$user->id);
            Mail::send('recpassword', $data, function($message) use($email){
            $message->to($email, 'Administrator of armproject.am')->subject
            ('Password recover');
            $message->from('lilit4020@gmail.com','Lilitik');
        });
    }
    function recoverpass(Request $r){
        $pass = Hash::make($r->input("password"));
        $rec_code = Usermodel::where("email",$r->eml)->first('recovercode');
        // dd($r->onetimecode);
            # code...

        $validate = Validator::make($r->all(),[
            "onetimecode"=>"required",
            "password"=>"min:8|required",
            "password2"=>"required|same:password",
        ]);

        if ($validate->fails())
        {
           return Redirect::back()->withErrors($validate)->withInput();
        }
        else{
        if ($rec_code->recovercode == $r->onetimecode) {
            Usermodel::where("email",$r->eml)->update(["password"=>$pass,"recovercode"=>""]);
            Session::flash("success","your password changed sucsessfuly");
                 return Redirect::to('/login');
                }
    }
    
    }
}
