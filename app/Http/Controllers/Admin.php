<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Validator;
use App\Usermodel;
use App\ProductModel;
use App\photomodel;
use App\NotifModel;
use Session;
use Illuminate\Support\Facades\Hash;
class Admin extends Controller
{
    function index(){

    }
    function allusers(){
        $users  =   Usermodel::get();
        // dd($users);
        return view("users")->with('title','allusers')->with('users',$users);
    }
    function admin(){
        // dd(Session('id'));
        $admins = Usermodel::where(['id' => Session('id')])->get();
        // dd($admins);
        return view("dashboard")->with('title','dashboard')->with('admins',$admins);
    }
    function pendingprods(){
        $prods = ProductModel::where(["status" => 0])->get();
        // dd($prods);
        return view("pendingprods")->with('title','pendingprods')->with('prods',$prods);
    }
    function verifyprod(Request $r){
        $id = $r->id;
        if ($r->type == 1) {
        Productmodel::where("id",$id)->update(["status"=>1]);
        NotifModel::insert(["product_id"=>$id,"user_id"=>$r->user_id,"status" =>1]);
        }else{
        Productmodel::where("id",$id)->delete();
       NotifModel::insert(["product_id"=>$id,"user_id"=>$r->user_id,"status" =>0]);
        }
    }
    function verifiedprods(){
        $prods = Productmodel::where(["status" => 1])->get();
        return view("verifiedprods")->with('title','verifiedprods')->with('prods',$prods);
    }
    function blockuser(Request $r){
        date_default_timezone_set("Asia/Tbilisi");
        $id = $r->id;
        $min = $r->min;
        Usermodel::where("id",$id)->update(["blocktime"=>time()+$min*60]);
  }
}
