<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Stripe\Error\Card;
use Cartalyst\Stripe\Stripe;
use App\CartModel;
use App\OrderModel;
use App\Order_detailsModel;
use App\ProductModel;
class AddMoneyController extends Controller
{
	public function payWithStripe()
	{	
			$sum = CartModel::where("user_id",session('id'))->get();
				$allsum = 0;
				foreach ($sum as $value) {
					$allsum = $allsum + ($value['piece']*$value->product->price);
				}
		return view('paywithstripe')->with('title','payment')->with('amount',$allsum);
	}
	public function postPaymentWithStripe(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'card_no' => 'required',
			'ccExpiryMonth' => 'required',
			'ccExpiryYear' => 'required',
			'cvvNumber' => 'required',
 //'amount' => 'required',
		]);
		$input = $request->all();
		if ($validator->passes()) { 
			$input = array_except($input,array('_token'));
			$stripe = Stripe::make('sk_test_pD7qGac8aMyW73AAK9fMyoCO00Ynz9wcG1');
			try {
				$token = $stripe->tokens()->create([
					'card' => [
						'number' => $request->get('card_no'),
						'exp_month' => $request->get('ccExpiryMonth'),
						'exp_year' => $request->get('ccExpiryYear'),
						'cvc' => $request->get('cvvNumber'),
					],
				]);
 // $token = $stripe->tokens()->create([
 // 'card' => [
 // 'number' => '4242424242424242',
 // 'exp_month' => 10,
 // 'cvc' => 314,
 // 'exp_year' => 2020,
 // ],
 // ]);
				if (!isset($token['id'])) {
					return redirect()->route('addmoney.paywithstripe');
				}
				$sum = CartModel::where("user_id",session('id'))->get();
				$allsum = 0;
				foreach ($sum as $value) {
					$allsum = $allsum + ($value['piece']*$value->product->price);
				}
				
				// dd($allsum);
				/*stex petqa harcum grenq vor yndhanur gumary vercnenq dnenq 10.49 i texy*/
				$charge = $stripe->charges()->create([
					'card' => $token['id'],
					'currency' => 'AMD',
					'amount' => $allsum,
					'description' => 'Add in wallet',
				]);

				if($charge['status'] == 'succeeded') {
					$sum2 = CartModel::where("user_id",session('id'))->get();
					$order = new OrderModel;
					$order->sum = $allsum;
					$order->user_id = session('id');
					$order->save();
					$order_id = $order->id;
					
					foreach ($sum2 as $value) {
						Order_detailsModel::insert(['order_id'=>$order_id,'product_id'=>$value->product_id,'price'=>$value->product['price'],'count'=>$value['piece']]);
						$count = ProductModel::where('id',$value->product_id)->get('count');
						// dd($count[0]->count);
						ProductModel::where('id',$value->product_id)->update(['count'=>$count[0]->count-$value['piece']]);
				}

 return redirect()->route('addmoney.paywithstripe');
} else {
	\Session::put('error','Money not add in wallet!!');
	return redirect()->route('addmoney.paywithstripe');
}
} catch (Exception $e) {
	\Session::put('error',$e->getMessage());
	return redirect()->route('addmoney.paywithstripe');
} catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
	\Session::put('error',$e->getMessage());
	return redirect()->route('addmoney.paywithstripe');
} catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
	\Session::put('error',$e->getMessage());
	return redirect()->route('addmoney.paywithstripe');
}
}
}
}
