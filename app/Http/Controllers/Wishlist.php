<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WishlistModel;
use App\ProductModel;
use App\CartModel;
use Illuminate\Support\Facades\Redirect;
use Session;


class Wishlist extends Controller
{
      function movetowishlist(Request $r){
        $id = $r->id;
        $ka = WishlistModel::where('user_id',session('id'))->where('product_id',$id)->get();
        if (count($ka) == 0) {   
        if ($r->type == 1) {
        WishlistModel::insert(['product_id' => $id, 'user_id' => session('id')]);
        CartModel::where('user_id',session('id'))->where('product_id',$id)->delete();
        }
        if ($r->type == 0) {
        WishlistModel::insert(['product_id' => $id, 'user_id' => session('id')]);
        }
      }
    }
    function wishpage(){
    	$wishlist = WishlistModel::where('user_id',session('id'))->get();
    	return view('wishlist')->with('title','WishList')->with('wishprods',$wishlist);
    }
}
