<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    public $table = "products";
    public $timestamps = false;

    function photos(){
    	return $this->hasMany("App\photomodel","product_id");
    }

    function user(){
    	return $this->belongsTo("App\Usermodel","user_id");
    }
    function feedbacks(){
    	return $this->hasMany("App\FeedModel","product_id");
    } 
  

}
