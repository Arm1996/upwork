<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedModel extends Model
{
   public $table = "feedback";
    public $timestamps = false;
      function user(){
    	return $this->belongsTo("App\Usermodel","user_id");
    }
}
