<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderModel extends Model
{
    public $table = "order";
    public $timestamps = false;
    
 function details(){
 	return $this->hasMany("App\Order_detailsModel","order_id");
}

}
