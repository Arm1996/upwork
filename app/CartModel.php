<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartModel extends Model
{
    public $table = "cart";
    public $timestamps = false;

    function product(){
    	return $this->belongsTo("App\ProductModel", "product_id");
    }

    function photos(){
    	return $this->belongsTo("App\photomodel","product_id", "product_id");
    }
    
}
