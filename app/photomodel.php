<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class photomodel extends Model
{
     public $table = "products_photos";
    public $timestamps = false;
}
