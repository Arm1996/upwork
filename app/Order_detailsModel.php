<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_detailsModel extends Model
{
     public $table = "order_details";
    public $timestamps = false;
}

 function product(){
    	return $this->belongsTo("App\Productmodel","product_id");
    }
 function user(){
 	return $this->belongsTo("App\Usermodel","user_id");
 }
