<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usermodel extends Model
{
    public $table = "user";
    public $timestamps = false;

    function cart(){
   	return $this->hasMany("App\CartModel","user_id");
    }

}
