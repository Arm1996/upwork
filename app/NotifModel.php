<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotifModel extends Model
{
     public $table = "notif";
   	 public $timestamps = false;

 function user(){
    	return $this->belongsTo("App\Usermodel","user_id");
    }
  function product(){
    	return $this->belongsTo("App\ProductModel", "product_id");
    }
}