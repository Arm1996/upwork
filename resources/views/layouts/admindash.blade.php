<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{$title}}</title>
		<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="{{asset('/css/style.css')}}">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>

<body>
    {{csrf_field()}}
	<nav class="navbar navbar-expand-sm bg-info navbar-light">
  <ul class="navbar-nav">
   <li class="nav-item active">
      <a class="nav-link" href="{{url('/admin')}}">Dashboard</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{url('/admin/users')}}">All Users</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{url('/admin/products')}}">Pending Products</a>
    </li>
    <li class="nav-item">
      <a class="nav-link " href="{{url('/admin/verifiedprods')}}" >Verified Products</a>
    </li>
    <li class="nav-item">
      <a class="nav-link btn btn-danger" href="{{url('logout')}}">Log out</a>
    </li>
  </ul>
</nav>
	@yield('content')


</body>

<script src="{{asset('/js/admin.js')}}"></script>
</html>