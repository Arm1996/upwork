<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{$title}}</title>
		<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="{{asset('/css/style.css')}}">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>
<body>
    {{csrf_field()}}
<!-- 	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <ul class="navbar-nav">
   <li class="nav-item active">
      <a class="nav-link" href="{{url('profile')}}">Home</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{url('newprod')}}">New product</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{url('products')}}">All products</a>
    </li>
    <li class="nav-item ">
      <a class="nav-link" href="{{url('myprod')}}">My products</a>
    </li>
      <li class="nav-item">
      <a class="nav-link" href="{{url('myorders')}}">My Orders</i></a>
    </li>
    <li class="nav-item">
      <a class="nav-link btn btn-danger" href="{{url('logout')}}">Log out</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{url('mycart')}}" title="My Cart"><i class="fab fa-opencart font-weight-bold"></i></a>
    </li>
      <li class="nav-item ">
      <a class="nav-link" href="{{url('wishlist')}}" title="Wishlist"><i class="fas fa-heart"></i></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{url('notif')}}"><i class="fas fa-bell note"></i></a>
    </li>
  </ul>
</nav> -->
/**************************
<!DOCTYPE html>
<html lang="en">
<head>
<title>Arm Shop</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Little Closet template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{asset('/css/styles/bootstrap-4.1.2/bootstrap.min.css')}}">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{asset('/css/styles/plugins/OwlCarousel2-2.2.1/owl.carousel.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('/css/styles/plugins/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('/css/styles/plugins/OwlCarousel2-2.2.1/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('/css/styles/main_styles.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('/css/styles/responsive.css')}}">
</head>
<body>

<!-- Menu -->

<div class="menu">

  <!-- Search -->
  <div class="menu_search">
    <form action="#" id="menu_search_form" class="menu_search_form">
      <button class="menu_search_button"><img src="{{asset('/css/images/search.png')}}" alt=""></button>
    </form>
  </div>
  <!-- Navigation -->
  <div class="menu_nav">
    <ul>
      <li><a href="#">Women</a></li>
      <li><a href="#">Men</a></li>
      <li><a href="#">Kids</a></li>
      <li><a href="#">Home Deco</a></li>
      <li><a href="#">Contact</a></li>
    </ul>
  </div>
  <!-- Contact Info -->
  <div class="menu_contact">
    <div class="menu_phone d-flex flex-row align-items-center justify-content-start">
      <div><div><img src="{{asset('/css/images/phone.svg')}}" alt="https://www.flaticon.com/authors/freepik"></div></div>
      <div>+1 912-252-7350</div>
    </div>
    <div class="menu_social">
      <ul class="menu_social_list d-flex flex-row align-items-start justify-content-start">
        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
      </ul>
    </div>
  </div>
</div>

<div class="super_container">

  <!-- Header -->

  <header class="header">
    <div class="header_overlay"></div>
    <div class="header_content d-flex flex-row align-items-center justify-content-start">
      <div class="logo">
        <a href="#">
          <div class="d-flex flex-row align-items-center justify-content-start">
            <div><img src="{{asset('/css/images/logo_1.png')}}" alt=""></div>
            <div>Arm Shop</div>
          </div>
        </a>  
      </div>
      <div class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></div>
      <nav class="main_nav">
        <ul class="d-flex flex-row align-items-start justify-content-start">
          <li class="active"><a href="{{url('profile')}}">Home</a></li>
          <li><a href="{{url('newprod')}}">New Product</a></li>
          <li><a href="{{url('products')}}">All Products</a></li>
          <li><a href="{{url('myprod')}}">My Products</a></li>
          <li><a href="{{url('myorders')}}">My Orders</a></li>
        </ul>
      </nav>
      <div class="header_right d-flex flex-row align-items-center justify-content-start ml-auto">
        <!-- Search -->
        <div class="header_search">
          <form action="#" id="header_search_form">
          </form>
        </div>
        <!-- User -->
        <div class="user"><a href="{{url('wishlist')}}"><div><img src="{{asset('/css/images/wish.png')}}" alt="https://www.flaticon.com/authors/freepik"><div>1</div></div></a></div>
        <!-- Cart -->
              <div class="user"><a href="{{url('mycart')}}"><div><img src="{{asset('/css/images/cart.svg')}}" alt="https://www.flaticon.com/authors/freepik"><div>1</div></div></a></div>


        <!-- <div class="cart"><a href="{{url('mycart')}}"><div><img class="svg" src="{{asset('/css/images/cart.svg')}}" alt="https://www.flaticon.com/authors/freepik"></div></a></div> -->
        <!-- Phone -->
        <div class="header_phone d-flex flex-row align-items-center justify-content-start">
          <div><a href="{{url('notif')}}"><div><i class="fas fa-bell note" style="font-size: 35px;"></i></div></a></div>
             <div><a href="{{url('logout')}}"><div><img src="{{asset('/css/images/logout.png')}}" alt="https://www.flaticon.com/authors/freepik" width="40" height="40"></div></a></div>
        </div>
      </div>
    </div>
  </header><br><br>


/*************************
	@yield('content')


</body>

<script src="{{asset('/js/script.js')}}"></script>
<script src="{{asset('/js/cart.js')}}"></script>


<script src="{{asset('/js/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('/js/styles/bootstrap-4.1.2/popper.js')}}"></script>
<script src="{{asset('/js/styles/bootstrap-4.1.2/bootstrap.min.js')}}"></script>
<script src="{{asset('/js/plugins/greensock/TweenMax.min.js')}}"></script>
<script src="{{asset('/js/plugins/greensock/TimelineMax.min.js')}}"></script>
<script src="{{asset('/js/plugins/scrollmagic/ScrollMagic.min.js')}}"></script>
<script src="{{asset('/js/plugins/greensock/animation.gsap.min.js')}}"></script>
<script src="{{asset('/js/plugins/greensock/ScrollToPlugin.min.js')}}"></script>
<script src="{{asset('/js/plugins/OwlCarousel2-2.2.1/owl.carousel.js')}}"></script>
<script src="{{asset('/js/plugins/easing/easing.js')}}"></script>
<script src="{{asset('/js/plugins/progressbar/progressbar.min.js')}}"></script>
<script src="{{asset('/js/plugins/parallax-js-master/parallax.min.js')}}"></script>
<script src="{{asset('/js/js/custom.js')}}"></script>

</html>