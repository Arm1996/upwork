@extends('layouts.user')

@section('content')
<h1>Wishlist</h1>
<div>
		  <table class="table table-danger table-hover">
    <thead>
      <tr>
        <th>Product Name</th>
        <th>Price</th>
        <th>Image</th>
        <th>Description</th>
        <th>Buy</th>
      </tr>
    </thead> 
     <tbody>
@foreach($wishprods as $value)
		<tr>
        <td>{{$value->product->name}}</td>
        <td>{{$value->product->price}}</td>
        <td><img src="{{asset('/prodimages/'.$value->photos->url)}}" width="200" height="100"></td>
        <td>{{$value->product->description}}</td>
        <td>
          <button class="btn btn-success adcrt" id="{{$value->product_id}}" type = '1'><i class="fas fa-cart-plus"></i></button>
        </td>

      </tr>

@endforeach
		 </tbody>
 		 </table>
	</div>
@endsection('content')