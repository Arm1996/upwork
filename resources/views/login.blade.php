@extends('layouts.main')

@section('content')
	{{Session::get('success')}}
	{{session('error')}}
	<form action="{{url('checkuser')}}" method="post">
		{{csrf_field()}}
		Email <input type="text" name="email" class="form-control">
		Password <input type="text" name="password" class="form-control">
		<button class="btn btn-success">Log In</button>
	</form>
		<a href="{{url('/signup')}}" class="btn btn-info mt-2">Sign Up!</a><br>
		<a href="{{url('/forgotpass')}}" class="btn btn-danger mt-2">Forgot Passwor?</a>
@endsection('content')
