@extends('layouts.admindash')

@section('content')
	<div>
		  <table class="table table-primary table-hover">
    <thead>
      <tr>
        <th>Product Name</th>
        <th>Count</th>
        <th>Price</th>
        <th>Image</th>
        <th>Description</th>
        <th>Status</th>
      </tr>
    </thead> 
     <tbody>
@foreach($prods as $value)
      <tr>
        <td>{{$value->name}}</td>
        <td>{{$value->count}}</td>
        <td>{{$value->price}}</td>
<!--         <td><img src="{{asset('/prodimages/'.$value->photos)}}" width="200" height="100"></td>
 -->        <td>{{$value->description}}</td>
        <td><button class="btn btn-success verify" id="{{$value->id}}" type="1" data-id="{{$value->user_id}}">Verify</button><button class="btn btn-danger verify ml-2" id="{{$value->id}}" type="0" data-id="{{$value->user_id}}">Dont Verify</button></td>
      </tr>
   
@endforeach
 </tbody>
  </table>
	</div>
@endsection('content')