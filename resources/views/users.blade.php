@extends('layouts.admindash')

@section('content')
	<div>
		  <table class="table table-warning table-hover">
    <thead>
      <tr>
        <th>Name</th>
        <th>Surname</th>
        <th>Age</th>
        <th>Email</th>
        <th>Avatar Picture</th>
        <th>Last Activity</th>
        <th>Status</th>
      </tr>
    </thead> 
     <tbody>
@foreach($users as $value)
<tr>
        <td>{{$value->name}}</td>
        <td>{{$value->surname}}</td>
        <td>{{$value->age}}</td>
        <td>{{$value->email}}</td>
        <td><img src="{{asset('/images/'.$value->photo)}}" width="100" height="100" alt="Avatarpic"></td>
        <td>{{$value->lastvisit}}</td>
        <td>
          <button class="btn btn-danger blokuser">Block</button>
          <div class="blockdiv">
            
          <input type="text" class="blockmin">
          <button class="btn btn-danger bloking" id="{{$value->id}}">Ok</button>
          </div>
        </td>
        <td>
          <i class="fas fa-envelope sms"></i>
          <div class="smsdiv">
            <textarea name="" id="" cols="30" rows="10"></textarea>
            <i class="far fa-paper-plane sendsms"></i>
          </div>
        </td>
      </tr>
@endforeach

</tbody>
  </table>
	</div>
@endsection('content')