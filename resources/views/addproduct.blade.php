@extends('layouts.user')

@section('content')
	<form action="{{url('add')}}" method="post" enctype="multipart/form-data">
	<h5>{{Session::get('success')}}</h5>
		{{csrf_field()}}
		{{$errors->first("name")}}
		Name: <input type="text" name="prname" class="form-control">
		{{$errors->first("price")}}
		Price: <input type="text" name="price" class="form-control">
		{{$errors->first("count")}}
		Count: <input type="text" name="count" class="form-control">
		{{$errors->first("desc")}}
		Description: <textarea name="desc" class="form-control"></textarea>
		Photo: <label for="files" class="form-control"><i class="fas fa-upload" style="font-size: 25px;"></i></label><input type="file" name="photo[]" multiple class="form-control" id="files" style="display: none;">
		<button class="btn btn-success mt-3" style="width: 100%">Add Product</button>
	</form>
@endsection('content')