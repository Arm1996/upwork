@extends('layouts.user')

@section('content')
<h1>Cart Page</h1>
<div>
		  <table class="table table-primary table-hover">
    <thead>
      <tr>
        <th>Product Name</th>
        <th>Price</th>
        <th>Count</th>
        <th>Image</th>
        <th>Description</th>
        <th>Buy</th>
      </tr>
    </thead> 
     <tbody>
@foreach($cartprods as $value)
	<tr>
        <td>{{$value->product->name}}</td>
        <td>{{$value->product->price}}</td>
        <td><i class="far fa-minus-square minus" id="{{$value->product_id}}"></i><span class="countspan">
          {{$value->piece}}
        </span>  <i class="far fa-plus-square plus" id="{{$value->product_id}}"></i></td>
        <td><img src="{{asset('/prodimages/'.$value->photos->url)}}" width="200" height="100"></td>
        <td>{{$value->product->description}}</td>
        <td>
          
          <button class="btn btn-success wishlist" type="0" id="{{$value->product_id}}">Move To Wishlist</button>
          <button class="btn btn-danger adcrt" type="2">Remove</button>
        </td>

      </tr>
@endforeach
<a href="{{url('addmoney/stripe')}}" class="btn btn-info gnel">Checkout</a>
	 </tbody>
  </table>
	</div>
@endsection('content')