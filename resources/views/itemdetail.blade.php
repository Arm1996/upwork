@extends('layouts.user')

@section('content')
<div class="itemdetailparentdiv">


  <div>
    <h1>{{$product->name}}</h1>

    <div id="carouselExampleIndicators" class="carousel slide w-25 mx-auto" data-ride="carousel">

      <div class="carousel-inner ayoo">
        @foreach($product->photos as $key=> $value)
        @if( $key==0)
        <div class="carousel-item active sliderdiv" >
          <i class="fas fa-times-circle delprodphoto" id="{{$value->id}}"></i>

          <img src="{{asset('/prodimages/'.$value->url)}}"  class="d-block w-100"><br>
        </div>
        @else
        <div class="carousel-item sliderdiv" >
          <i class="fas fa-times-circle delprodphoto" id="{{$value->id}}"></i>
          <img src="{{asset('/prodimages/'.$value->url)}}"  class="d-block w-100"><br>
        </div>
        @endif
        @endforeach		
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <p>Desc: {{$product->description}}</p>
    <p>Count: {{$product->count}}</p>
    <p>Price: {{$product->price}}</p>
    <button class="btn btn-success edit1" id="{{$product->user_id}}" data-id = "{{session('id')}}">Edit</button>

  </div>
  <div>
    <!-- {{$errors->first("name")}} -->
    <div class="editdiv2" style="display: {{(Session::has('display'))? 'block' : 'none'}}">
      <form action="{{url('edit1')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="hidden" name="id" value="{{$product->id}}">
              <h6 class="error">{{$errors->first("name")}}</h6>
        Name:  <input type="text" value="{{$product->name}}" name="name" class="form-control">
              <h6 class="error">{{$errors->first("count")}}</h6>
        Count:  <input type="text" value="{{$product->count}}" name="count" class="form-control">
              <h6 class="error">{{$errors->first("price")}}</h6>
        Price:  <input type="text" value="{{$product->price}}" name="price" class="form-control">
              <h6 class="error">{{$errors->first("desc")}}</h6>
        Description:  <textarea name="desc" class="form-control">{{$product->description}}</textarea>
        Add photo: <input type="file" name="photo1[]" multiple class="form-control">
        <button class="btn btn-info savedit mt-3">Save</button>
      </form>
    </div>
  </div>
</div>
@endsection('content')





