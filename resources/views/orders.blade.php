@extends('layouts.user')
@section('content')
<div>
		  <table class="table table-warning table-primary">
    <thead>
      <tr>
        <th>Order Price</th>
        <th>Order Date</th>
        <th>Order Details</th>
      </tr>
    </thead> 
     <tbody>
@foreach($order as $value)
<tr>
        <td>{{$value->sum}}</td>
        <td>{{$value->time}}</td>
        <td><a href="{{url('orders/details/'.$value->id)}}" class="btn btn-info" id="{{$value->id}}">Details</a></td>
      </tr>
@endforeach

</tbody>
  </table>
	</div>
@endsection('content')