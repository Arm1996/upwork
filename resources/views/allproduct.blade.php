@extends('layouts.user')

@section('content')
<div class="row" style="width: 100%">

@foreach($prods as $value)
		<div class="col-xl-2 col-md-4 mt-5">
						<div class="product" style="width: 100%">
							<div class="product_image"><img src="{{asset('/prodimages/'.$value->photos[0]->url)}}" width="300" height="200" alt=""></div>
							<div class="product_content">
								<div class="product_info d-flex flex-row align-items-start justify-content-start">
									<div>
										<div>
											<div class="product_name">{{$value->name}}</div>
											<!-- <div class="product_category">In <a href="category.html">Category</a></div> -->
										</div>
									</div>
									<div class="ml-auto text-right">
										<div class="rating_r rating_r_4 home_item_rating"><i>Seller:{{$value->user->name}}</i></div>
										<div class="product_price text-right">{{$value->price}}<span>\{{$value->count}}</span></div>
									</div>
								</div>
								<div class="product_buttons">
									<div class="text-right d-flex flex-row align-items-start justify-content-start">
										<div class="product_button product_fav text-center d-flex flex-column align-items-center justify-content-center adcrt" id="{{$value->id}}" type='2'>
											<div><div><img src="{{asset('/css/images/cart.svg')}}" class="svg" alt=""><div>+</div></div></div>
										</div>
										<div class="product_button product_cart text-center d-flex flex-column align-items-center justify-content-center wishlist" type="1" id="{{$value->id}}">
											<div><div><img src="{{asset('/css/images/heart_2.svg')}}" class="svg" alt=""><div>+</div></div></div>
										</div><br>
										<div class="product_button product_cart text-center d-flex flex-column align-items-center justify-content-center rev" style="border: 1px solid #ededed" type="1" id="rev">
											<div class="rev"><div><img src="{{asset('/css/images/feed.png')}}" class="svg rev" alt=""><div>{{count($value->feedbacks)}}</div></div></div>
										</div>											
									</div>
								</div>
									<div class="feedbacksdiv">
												@foreach($value->feedbacks as $key)
												<h4 style="color: black">{{$key->user->name}} {{$key->user->surname}}</h4>
												<h6>{{$key->text}}</h6>
												@endforeach
											</div>
							</div>
						</div>
					</div>

@endforeach
</div>
@endsection('content')



