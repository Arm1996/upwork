@extends('layouts.admindash')

@section('content')
	<div>
		  <table class="table table-primary table-hover">
    <thead>
      <tr>
        <th>Product Name</th>
        <th>Count</th>
        <th>Price</th>
        <th>Image</th>
        <th>Description</th>
      </tr>
    </thead> 
     <tbody>
 @foreach($prods as $value)
	 <tr>
        <td>{{$value->name}}</td>
        <td>{{$value->count}}</td>
        <td>{{$value->price}}</td>
        <td><img src="{{asset('/prodimages/'.$value->photos[0]->url)}}" width="200" height="100"></td>
        <td>{{$value->description}}</td>
      </tr>

 @endforeach
	 </tbody>
  </table>
	</div>
@endsection('content')