@extends('layouts.main')

@section('content')

	<form action="{{url('save')}}" method="post" class="m-3 w-25">
		{{csrf_field()}}
			{{$errors->first("name")}}
		Name <input type="text" name="name" class="form-control" value="{{old('name')}}">
			{{$errors->first("surname")}}
		Surname <input type="text" name="surname" class="form-control" value="{{old('surname')}}">
			{{$errors->first("age")}}
		Age	<input type="text" name="age" class="form-control" value="{{old('age')}}">
			{{$errors->first("email")}}
		Email <input type="text" name="email" class="form-control" value="{{old('email')}}">
			{{$errors->first("password")}}
		Password <input type="password" name="password" class="form-control" value="{{old('password')}}">
			{{$errors->first("password2")}}
		Confirm Password <input type="password" name="password2" class="form-control" value="{{old('password2')}}">
			{{$errors->first("type")}}
		<!-- Type <select name="type" id="">
				<option value="" selected="" disabled="">Who are you?</option>
				<option value="Employer">Employer</option>
				<option value="Developer">Developer</option>
			 </select> --><br>
		<button class="btn btn-success mt-3">Sign Up</button>
	</form>
@endsection('content')
