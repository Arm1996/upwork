@extends('layouts.user')
@section('content')
<div>
		  <table class="table table-warning table-hover">
    <thead>
      <tr>
        <th>Product Name</th>
        <th>Product Price</th>
        <th>Description</th>
        <th>Status</th>
        <th>Message</th>
      </tr>
    </thead> 
     <tbody>
@foreach($notif as $value)
<tr>
        <td>{{$value->product->name}}</td>
        <td>{{$value->product->price}}</td>
        <td>{{$value->product->description}}</td>
        <td>
          @if($value->status == 0)
          Deleted
          @else
          Verified
          @endif
        </td>
        <td>{{$value->text}}</td>
        <td><i class="far fa-times-circle closenote" id="{{$value->id}}"></i>
        </td>
      </tr>
@endforeach

</tbody>
  </table>
	</div>
@endsection('content')