@extends('layouts.user')

@section('content')
	<div>
		<div style="text-align: center;">
						<h1>{{$user['name']}} {{$user['surname']}}</h1><br>
		<img src="{{asset('/images/'.$user->photo)}}" class="avdiv">
	
	<form action="{{url('upload')}}" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<label for="imageInput"><i class="fas fa-camera-retro" style="font-size: 40px;cursor: pointer;"></i></label>
		<input type="file" name="input_img" id="imageInput"><br>
		<button name="upl" class="btn btn-success">Change Avatar</button>
	</form><br>
	<i class="fas fa-user-edit editdata" style="font-size: 40px;cursor: pointer;"></i>
<!-- 	<button class="editdata">Edit</button><br>
 -->		<div class="editdiv">
			Name: <input type="text" class="username form-control">
			Surname: <input type="text" class="usersurname form-control">
			<button class="btn btn-success mt-3 savechanges">Change</button>
		</div>
		</div>

	

	</div>

@endsection('content')
