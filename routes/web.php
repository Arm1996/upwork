<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login')->with('title','login');
});
Route::get('/signup','User@index');
Route::post('/save','User@add');
Route::get('login','User@login');
Route::post('/checkuser','User@login2');
Route::get('profile','User@profile')->middleware("islogin");
Route::get('logout','User@logout');
Route::post('upload','User@upload');
Route::post('/edit','User@edit');
Route::get('/newprod','Product@addproduct')->middleware("islogin");;
Route::post('add','Product@add');
Route::get('/products','Product@allprod')->middleware("islogin");;
Route::get('myprod','Product@myprod')->middleware("islogin");;
Route::get('product_item/{id}','Product@product_item');
Route::post('/edit1','Product@editprod');
Route::post('/deleteprod','Product@deleteprod');
Route::post('/delprodpic','Product@deletprodpic');
Route::get('/admin','Admin@admin')->middleware("islogin");
Route::get('/admin/users','Admin@allusers');
Route::get('/admin/products','Admin@pendingprods');
Route::post('/admin/verify','Admin@verifyprod');
Route::get('/admin/verifiedprods','Admin@verifiedprods');
Route::post('/addtocart','Cart@addtocart');
Route::get('/mycart','Cart@cartpage');
Route::post('/cart/mincount','Cart@mincount');
Route::post('/cart/pluscount','Cart@pluscount');
Route::post('/cart/movetowish','Wishlist@movetowishlist');
Route::get('/wishlist','Wishlist@wishpage');
Route::post('/admin/blockuser','Admin@blockuser');
Route::get('/verify/{hash}/{id}','User@verify');
Route::get('/forgotpass','User@forgotpass');
Route::post('/sendemail','User@sendemail');
Route::post('/recoverpass','User@recoverpass');
Route::get('/notif','Notif@notification');
Route::post('/getnotif','Notif@getnotif');
Route::post('/delnotif','Notif@delnotif');
Route::get('myorders','Orders@getview');
Route::get('orders/details/{id}','Orders@details');
Route::post('/feedback','Orders@sendfeed');
Route::get('/home','User@homepage');


Route::get('addmoney/stripe', array('as' => 'addmoney.paywithstripe','uses' => 'AddMoneyController@payWithStripe'));
Route::post('addmoney/stripe', array('as' => 'addmoney.stripe','uses' => 'AddMoneyController@postPaymentWithStripe'));
