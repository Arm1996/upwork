const _token2 = $("input[name='_token']").val()

$(".minus").click(function(){
	let id = $(this).attr("id")
	let a =+ $(this).parent().find('.countspan').html()
	let t = $(this)
	$.ajax({
		url: '/cart/mincount',
		type: 'post',
		data: {"_token" : _token2, id:id},
		success:function(r){
			// let a =+ $(".countspan").html()
			if (r=="true") {
				a--
			}
			t.parent().find(".countspan").html(a)
			
		}
	})
});

$(".plus").click(function(){
	let id = $(this).attr("id")
	let a =+ $(this).parent().find('.countspan').html()
	let t = $(this)
	$.ajax({
		url: '/cart/pluscount',
		type: 'post',
		data: {"_token" : _token2, id : id},
		success:function(r){
			console.log(r)
			// let a =+ $(".countspan").html()
			if (r=="true") {
				a++	
			}
			t.parent().find(".countspan").html(a)
		}
	})
});

$(".wishlist").click(function(){
	alert("wish")
	let id = $(this).attr("id")
	let type = $(this).attr("type")
	$.ajax({
		url: '/cart/movetowish',
		type: 'post',
		data:{ "_token" : _token2, id : id},
		success:function(r){
			console.log(r)
		}
	})
	$(this).parent().find('td').remove()
});